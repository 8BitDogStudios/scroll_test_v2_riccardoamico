﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ThumbnailManager:MonoBehaviour
{
    public static ThumbnailManager current;
    public Transform container;
	public GameObject prefab;
    [Range(1, 30)]
    public float SlideMultiplier = 7f;
    [Range(1, 3000)]
    public float ForceMultiplier = 7f;


    private int MinThumbnailOnSceen, MaxThumbnailOnSceen;

    private Vector2 Offset;
    private int NumOfThumbnailVO = 50;
    private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
    private List<GameObject> InvisibleRows = new List<GameObject>();
	void Start () {
        current = this;
        MinThumbnailOnSceen = 0;
        MaxThumbnailOnSceen = 45;
        Offset = container.transform.position;
        createThumbnailVOList();
		createThumbnailPrefabs();
    }
	
	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		for (int i=0; i< 1000; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
	}

	private void createThumbnailPrefabs() {
		GameObject gameObj;
		for (int i = 0; i < NumOfThumbnailVO; i++) {
			gameObj = (GameObject)Instantiate(prefab);
			gameObj.transform.SetParent(container.transform.GetChild((int)i / 5).transform.GetChild((int) i % 5).transform, false);
            gameObj.GetComponent<RectTransform>().sizeDelta = new Vector2(100,100);
            gameObj.GetComponent<RectTransform>().localPosition = new Vector2(0,0);
			gameObj.GetComponent<Thumbnail>().thumbnailVO = _thumbnailVOList[i];
        }
	}

    public void MoveRowsPosition(Vector2 Direction)
    {
        container.transform.Translate((Direction * Time.deltaTime) * SlideMultiplier);
    }

    public void MoveRowsWithForce(Vector2 Direction)
    {
        container.GetComponent<Rigidbody2D>().AddForce(((Direction * Time.deltaTime) * ForceMultiplier), ForceMode2D.Impulse);
        var v = container.GetComponent<Rigidbody2D>().velocity;
        GameObject.Find("Textt").GetComponent<Text>().text = v.sqrMagnitude.ToString();
        if (v.sqrMagnitude > (11 * 11))
        { // Equivalent to: rigidbody.velocity.magnitude > maxVelocity, but faster.
          // Vector3.normalized returns this vector with a magnitude 
          // of 1. This ensures that we're not messing with the 
          // direction of the vector, only its magnitude.
            container.GetComponent<Rigidbody2D>().velocity = v.normalized * 11;
        }
    }

    public void AddInvisibleRow(GameObject Row)
    {
        InvisibleRows.Add(Row);
    }

    public void SubInvisibleRow(GameObject Row)
    {
        InvisibleRows.Remove(Row);
    }

    public List<ThumbnailVO> GetListOfThumbnailVO(DirectionType dir)
    {
        switch (dir)
        {
            case DirectionType.down:
                MinThumbnailOnSceen -= 5;
                MaxThumbnailOnSceen -= 5;
                return _thumbnailVOList.GetRange(MinThumbnailOnSceen, 5);
            case DirectionType.up:
                MinThumbnailOnSceen += 5;
                MaxThumbnailOnSceen += 5;
                return _thumbnailVOList.GetRange(MaxThumbnailOnSceen - 5, 5);
        }
        return null;
    }

    private void FixedUpdate()
    {
        if (InvisibleRows.Count > 0)
        {
            if (Offset.y > container.transform.position.y)
            {
                foreach (GameObject row in InvisibleRows)
                {
                    float newp = GetComponentsInDirectChildren(container).Where(r => r.name != "Container" && r.GetComponent<RowManager>().isVisibleOnScreen).ToList().Max(r => r.localPosition.y);
                    row.GetComponent<RectTransform>().localPosition = new Vector3(0, newp + 210, 0);
                    row.GetComponent<RowManager>().dir = DirectionType.down;
                }
            }
            else if (Offset.y < container.transform.position.y)
            {
                foreach (GameObject row in InvisibleRows)
                {
                    float newp = GetComponentsInDirectChildren(container).Where(r => r.name != "Container" && r.GetComponent<RowManager>().isVisibleOnScreen).ToList().Min(r => r.localPosition.y);
                    row.GetComponent<RectTransform>().localPosition = new Vector3(0, newp - 210, 0);
                    row.GetComponent<RowManager>().dir = DirectionType.up;
                }
            }
        }

        if (container.GetComponent<RectTransform>().localPosition.y < 0)
        {
            container.GetComponent<RectTransform>().localPosition = Vector2.zero;
            container.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        else if (container.GetComponent<RectTransform>().localPosition.y > 40300)
        {
            container.GetComponent<RectTransform>().localPosition = new Vector3(0, 40300, 0);
            container.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }

        Offset = container.transform.position;
    }


    public List<RectTransform> GetComponentsInDirectChildren(Transform containerT)
    {
        List<RectTransform> ListRT = new List<RectTransform>();
        for (int i = 0; i < containerT.childCount; ++i)
        {
            RectTransform rt = containerT.GetChild(i).GetComponent<RectTransform>();
            if (rt != null)
                ListRT.Add(rt);
        }
        return ListRT;
    }
}
