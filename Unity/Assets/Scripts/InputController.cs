﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class InputController : MonoBehaviour
{
    private Vector2 Offset;

    void FixedUpdate()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Offset = Input.mousePosition;
        }
        else if(Input.GetMouseButton(0))
        {
            if (Offset.y > Input.mousePosition.y)
            {
                ThumbnailManager.current.MoveRowsPosition(Vector2.down);
                Offset = Input.mousePosition;
            }
            else if (Offset.y < Input.mousePosition.y)
            {
                ThumbnailManager.current.MoveRowsPosition(Vector2.up);
                Offset = Input.mousePosition;
            }
            
        }
        else if(Input.GetMouseButtonUp(0))
        {
            if (Mathf.Abs(Offset.y + (Input.mousePosition.y * (-1))) > 20)
            {
                if (Offset.y > Input.mousePosition.y)
                {
                    ThumbnailManager.current.MoveRowsWithForce(Vector2.down);
                }
                else if (Offset.y < Input.mousePosition.y)
                {
                    ThumbnailManager.current.MoveRowsWithForce(Vector2.up);
                }
            }
        }
	}
}
