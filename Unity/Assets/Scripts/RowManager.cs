﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowManager : MonoBehaviour {

    public bool isVisibleOnScreen = true;
    private Plane[] planes;
    private BoxCollider2D bc2D;
    public DirectionType dir;

    private void Start()
    {
        planes = GeometryUtility.CalculateFrustumPlanes(Camera.main);
        bc2D = GetComponent<BoxCollider2D>();
    }

    void FixedUpdate()
    {
        if (!GeometryUtility.TestPlanesAABB(planes, bc2D.bounds) && isVisibleOnScreen)
        {
            ThumbnailManager.current.AddInvisibleRow(this.gameObject);
            isVisibleOnScreen = false;
        }
        else if (GeometryUtility.TestPlanesAABB(planes, bc2D.bounds) && !isVisibleOnScreen)
        {
            ThumbnailManager.current.SubInvisibleRow(this.gameObject);
            isVisibleOnScreen = true;
            UpdateChildren();
        }
    }

    public void UpdateChildren()
    {
        List<ThumbnailVO> thumbnailVOList = ThumbnailManager.current.GetListOfThumbnailVO(dir);

        for (int i = 0; i < 5; i++)
        {
            transform.GetChild(i).transform.GetChild(0).GetComponent<Thumbnail>().thumbnailVO = thumbnailVOList[i];
        } 
    }


}

public enum DirectionType
{
    down = 0,
    up = 1
}
